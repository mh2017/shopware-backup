# Shopware Backup Tool

This simple Shellscript will make your shopware backup easy.


# Usecase

You want to update your shopware installation and want a database dump and file backup

## Functions

**Backup**
- Sync all files and folders for the specific shopware version (currently only version 5.6.x) to your specified path
- Dump your full shopware database and save it in your specified path

**Recover**
- Currently only recovers the filesystem, since the old database might want to be kept -> use the dump to import it with mysql .... command