#!/bin/bash
. main.conf

echo $SHOPWARE_DIR
today=$(date +"%m_%d_%Y")


function backup {
  echo "Backup was done on the $today of shopware located in $SHOPWARE_DIR" >> backup.log
  mkdir $BACKUP_DIR"shopware_backup_$today"
  rsync -avz --filter="merge filter.txt" $SHOPWARE_DIR $BACKUP_DIR"shopware_backup_$today/files/"

  # Database options
  # --lock-tables
  # --default-character-set=utf8
  mkdir $BACKUP_DIR"shopware_backup_$today/db"
  mysqldump -u$DBUSER -p$DBPW $DB > $BACKUP_DIR"shopware_backup_$today/db/backup.sql"
}

function recover {
  echo "Recovery was done on the $today of shopware located in $SHOPWARE_DIR" >> backup.log
  rsync -avz --delete --filter="merge filter.txt" $BACKUP_DIR"shopware_backup_$today/files/" $SHOPWARE_DIR
}


ESC=$(printf "\e")
PS3="$ESC[44m $ESC[97m $ESC[1m Please choose your option: $ESC[0m"
options=("Backup" "Recover" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Backup")
            backup
            break
            ;;
        "Recover")
            recover
            break
            ;;
        "Quit")
            echo "Bye,bye..."
            exit
            ;;
        *) echo invalid option;;
    esac
done
